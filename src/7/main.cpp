#include <iostream>
#include <cmath>

bool isPrime(unsigned number);

int main() {
	//Appart from two, all the prime numbers are odd, so we'll skip iterating over it
	unsigned i = 3, primeCount = 1;
	while (primeCount < 10'001)
		if (isPrime(i++))
			primeCount++;
	//i is incremented by postfix, so the 10,001st prime caught is lost
	std::cout << --i << std::endl;
	return 0;
}

/// <summary>
/// Returns whether a number is prime or composite.
/// </summary>
/// <param name="number"></param>
/// <returns></returns>
bool isPrime(unsigned number) {
	unsigned root = std::sqrt(number);

	if (number < 2)
		return false;

	for (unsigned i = 2; i <= root; i++)
		if (number % i == 0)
			return false;

	return true;
}

