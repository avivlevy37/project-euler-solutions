#include <iostream>
#include <cmath>

int main() {
	unsigned sumOfSquares = 0, squareOfSum = 0;
	for (unsigned i = 1; i <= 100; i++) {
		sumOfSquares += std::pow(i, 2);
		squareOfSum += i;
	}
	squareOfSum = std::pow(squareOfSum, 2);
	std::cout << squareOfSum - sumOfSquares << std::endl;
	return 0;
}