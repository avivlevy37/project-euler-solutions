#include <iostream>

/// <summary>
/// Returns whether three numbers make a Pythagorean Triplet, I.E: a^2 + b^2 = c^2.
/// </summary>
/// <param name="a"></param>
/// <param name="b"></param>
/// <param name="c">The hypotenuse of the triangle</param>
/// <returns></returns>
inline bool isPythagoreanTriplet(unsigned a, unsigned b, unsigned c) {
	return a*a + b*b == c*c;
}

int main() {
	unsigned a = 1, b, c;
	while (true) {
		b = 1;
		do {
			c = 1'000 - a - b;
			if (isPythagoreanTriplet(a, b, c))
				goto FoundTriplet;
			b++;
		} while (c);
		a++;
	}
FoundTriplet:
	std::cout << "a=" << a << ", b=" << b << ", c=" << c << std::endl <<
		a * b * c << std::endl;
	return 0;
}
