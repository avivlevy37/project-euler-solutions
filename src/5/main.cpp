#include <iostream>

int main() {
	unsigned number = 0;
	bool divisibleByAll = false;
	while (!divisibleByAll) {
		number += 20; //Let the jumps be as little as the largest divisor
		divisibleByAll = true;
		for (unsigned i = 2; i <= 20 && divisibleByAll; i++)
			if (number % i)
				divisibleByAll = false;
	}
	std::cout << number << std::endl;
	return 0;
}
