#include <iostream>
#include <vector>

/// <summary>
/// Using static memoization, calculates the nth Fibonacci term, starting from 0
/// </summary>
unsigned fibonacci(unsigned n) {
	static std::vector<unsigned> terms = { 1, 2 };

	if (n < terms.size())
		return terms[n];

	if (n == terms.size()) {
		terms.push_back(terms[n - 1] + terms[n - 2]);
		return terms[n];
	}

	return fibonacci(n - 1) + fibonacci(n - 2);
}

int main() {
	unsigned n = 0, sum = 0, term;
	do {
		term = fibonacci(n++);
		if (term % 2 == 0)
			sum += term;
	} while (term <= 4'000'000);

	std::cout << sum << std::endl;
	return 0;
}