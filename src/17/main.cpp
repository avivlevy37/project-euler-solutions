#include <iostream>
#include <string>
#include <algorithm>
using std::string;

/// <summary>
/// Converts a two-digit number into words. E.G: 1 becomes "one", 53 becomes "fifty-three", and so on.
/// </summary>
/// <param name="number">A non-negative number; will be treated as though it only had two digits (0-99).</param>
/// <returns>A string, representing the entered number in words.</returns>
string twoDigitNumberToWords(unsigned number) {
	const const char* DIGITS_AND_TEENS[] = {
		"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
		"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"
	};
	const const char* TENS[] = { "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

	number %= 100;  //Enforce that this is a 2-digit number.
	if (number < 20)
		return DIGITS_AND_TEENS[number];

	string inWords = TENS[number / 10 - 2];
	unsigned onesDigit = number % 10;
	if (onesDigit) {
		inWords += "-";
		inWords += DIGITS_AND_TEENS[onesDigit];
	}
	return inWords;
}

/// <summary>
/// Converts a three-digit number into words. E.G: 1 becomes "one", 253 becomes "two hundred and fifty-three", and so on.
/// </summary>
/// <param name="number">A non-negative number; will be treated as though it only had three digits (0-999).</param>
/// <returns>A string, representing the entered number in words.</returns>
string threeDigitNumberToWords(unsigned number) {
	const const char* HUNDREDS[] = {
		"", "one hundred", "two hundred", "three hundred", "four hundred", "five hundred",
		"six hundred", "seven hundred", "eight hundred", "nine hundred"
	};

	number %= 1'000; //Enforce that this is a 3-digit number.
	string inWords = HUNDREDS[number / 100];
	if (number % 100) {
		if (!inWords.empty())
			inWords += " and ";
		inWords += twoDigitNumberToWords(number);
	}
	return inWords;
}

/// <summary>
/// Converts a three-digit number into words. E.G: 1 becomes "one", 1253 becomes "one thousand, two hundred and fifty-three", and so on.
/// </summary>
/// <param name="number">A non-negative number.</param>
/// <returns>A string, representing the entered number in words.</returns>
string numberToWords(unsigned number) {
	const const char* postfixes[] = { "", " thousand", " million", " billion" };
	if (!number)
		return "zero";
	string inWords;
	for (const char* postfix : postfixes) {
		if (!number)
			break;
		if (number % 1000 == 0) { //If that segment is blank, no action is needed
			number /= 1000;
			continue;
		}
		inWords += ", ";
		inWords += threeDigitNumberToWords(number);
		inWords += postfix;
		number /= 1000;
	}
	return inWords.erase(0, 2); //Delete the first ", "
}


int main() {
	size_t length = 0;
	for (int i = 1; i <= 1000; i++) {
		string inWords = numberToWords(i);
		//Count the number of letters in string
		length += std::count_if(inWords.begin(), inWords.end(),
			[](char chr) { return 'a' <= chr && chr <= 'z'; });
	}
	std::cout << length << std::endl;
	return 0;
}
