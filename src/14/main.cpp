#include <iostream>

inline unsigned long nextCollatzTerm(unsigned long number) {
	return number % 2 ? 3 * number + 1 : number / 2;
}

unsigned collatzSequenceLength(unsigned long number) {
	unsigned length = 1;
	while (number != 1) {
		number = nextCollatzTerm(number);
		length++;
	}
	return length;
}

int main() {
	unsigned maxLength = 0;
	int maxLengthStartingNumber = -1;
	for (int i = 1; i < 1'000'000; i++) {
		unsigned length = collatzSequenceLength(i);
		if (length > maxLength) {
			maxLength = length;
			maxLengthStartingNumber = i;
		}
	}
	std::cout << maxLengthStartingNumber << std::endl;
	return 0;
}
