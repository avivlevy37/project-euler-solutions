#include <iostream>
#include <cmath>

/// <summary>
/// Revereses the digit order of a number.
/// </summary>
/// <param name="number"></param>
/// <returns></returns>
unsigned reverse(unsigned number) {
	unsigned decimalPlace = std::log10(number), reversed = 0;
	while (number) {
		reversed += (number % 10) * std::pow(10, decimalPlace--);
		number /= 10;
	}
	return reversed;
}

/// <summary>
/// Checks whether a number is a palindrome
/// </summary>
/// <param name="number"></param>
/// <returns></returns>
inline bool isPalindrom(unsigned number) {
	return number == reverse(number);
}

int main() {
	unsigned max = 0;
	for (unsigned i = 100; i < 1000; i++)
		for (unsigned j = 100; j < 1000; j++) {
			unsigned product = i * j;
			if (product > max && isPalindrom(product))
				max = product;
		}
	std::cout << max << std::endl;
	return 0;
}