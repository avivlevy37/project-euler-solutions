#include <iostream>
#include <functional>
#include <boost/multiprecision/cpp_int.hpp>
using boost::multiprecision::uint1024_t;

/// <summary>
/// Recursively counts the number of possible routes from the top left to the bottom right of a grid, when only being able to move right or down.
/// </summary>
/// <param name="rows">The height of the grid, y-axis</param>
/// <param name="cols">The width of the grid, x-axis</param>
/// <returns></returns>
uint1024_t countLatticePaths(int rows, int cols) {
	uint1024_t routesFound = 0;
	std::function<void(int, int)> navigateRecursively = [&](int y, int x) {
		if (y > rows || x > cols) //Exceeded the boundaries with no route found
			return;
		if (y == rows || x == cols) { //Reached the bottom right; route found!
			routesFound++;
			return;
		}
		navigateRecursively(x + 1, y);
		navigateRecursively(x, y + 1);
	};
	navigateRecursively(0, 0);
	return routesFound;
}

int main() {
	std::cout << countLatticePaths(2, 2) << std::endl;
	std::cout << countLatticePaths(20, 20) << std::endl;
	return 0;
}
