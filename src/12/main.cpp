#include <iostream>
#include <boost/multiprecision/cpp_int.hpp>
using boost::multiprecision::uint1024_t;


unsigned factorCount(uint1024_t number) {
	unsigned count = 0;
	uint1024_t stop = boost::multiprecision::sqrt(number);
	for (uint1024_t i = 1; i <= stop; i++) //Only iterating over unique factors
		if (number % i == 0)
			count += 2; //Each factor found has a counterpart, E.G: 1*28=28, 2*14=28
	return count;
}

int main() {
	uint1024_t triangleNumber = 1, jump = 2;
	while (factorCount(triangleNumber) <= 500)
		triangleNumber += jump++;
	std::cout << triangleNumber << std::endl;
	return 0;
}
