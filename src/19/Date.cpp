#include "Date.h"
#include <exception>
#include <algorithm>
using std::exception;
#define MONTH_LENGTHS (isLeapYear(_year) ? LEAP_YEAR_MONTH_LENGTHS : COMMON_YEAR_MONTH_LENGTHS)

Date Date::knownSunday = Date(31, DECEMBER, 1899);

Date::Date(const Date& other) {
	*this = other;
}

Date::Date(byte day, Month month, int year) :
	_day(day), _month(month), _year(year) {
	long long differenceInDays = 0;
	Date copy;

	if (!(JANUARY <= month && month <= DECEMBER))
		throw exception("Invalid month!");
	if (!(1 <= day && day <= MONTH_LENGTHS[month]))
		throw exception("Invalid day!");

	switch (compare(knownSunday)) {
	case EARLIER_THAN:
		copy = *this;
		while (copy.compare(knownSunday) != EQUALS_TO) {
			differenceInDays--;
			copy++;
		}
		break;

	case LATER_THAN:
		copy = knownSunday;
		while (copy.compare(*this) != EQUALS_TO) {
			differenceInDays++;
			copy++;
		}
		break;
	}
	_dayOfTheWeek = (DayOfTheWeek)((differenceInDays % DAYS_IN_A_WEEK + DAYS_IN_A_WEEK) % DAYS_IN_A_WEEK);
}

Date& Date::operator=(const Date& right) {
	_day = right._day;
	_month = right._month;
	_year = right._year;
	_dayOfTheWeek = right._dayOfTheWeek;
	return *this;
}

bool Date::operator==(const Date& right) const {
	return compare(right) == EQUALS_TO;
}

bool Date::operator!=(const Date& right) const {
	return !(operator==(right));
}

bool Date::operator>(const Date& right) const {
	return compare(right) == LATER_THAN;
}

bool Date::operator<(const Date& right) const {
	return compare(right) == EARLIER_THAN;
}

Date& Date::operator++() {
	//Circularly increment the day of the week
	_dayOfTheWeek = _dayOfTheWeek == SATURDAY ? SUNDAY : (DayOfTheWeek)(_dayOfTheWeek + 1);
	if (++_day > MONTH_LENGTHS[_month]) {
		_day = 1;
		if (_month == DECEMBER) {
			_month = JANUARY;
			_year++;
		}
		else
			_month = (Month)(_month + 1);
	}
	return *this;
}

Date Date::operator++(int) {
	return operator++();
}

byte Date::day() const {
	return _day;
}

Month Date::month() const {
	return _month;
}

int Date::year() const {
	return _year;
}

DayOfTheWeek Date::dayOfTheWeek() const {
	return _dayOfTheWeek;
}

bool Date::isLeapYear(int year) {
	if (year % 400 == 0)
		return true;
	if (year % 100 == 0) //Centurty years, except for those divisible by 400, are not leap years
		return false;
	return year % 4 == 0;
}

Date::CompareResult Date::compare(const Date& other) const {
	if (_year > other._year)
		return LATER_THAN;
	if (_year < other._year)
		return EARLIER_THAN;
	//Otherwise, both dates are the same year
	if (_month > other._month)
		return LATER_THAN;
	if (_month < other._month)
		return EARLIER_THAN;
	//Otherwise, both dates are the same month
	if (_day > other._day)
		return LATER_THAN;
	if (_day < other._day)
		return EARLIER_THAN;
	return EQUALS_TO;
}
