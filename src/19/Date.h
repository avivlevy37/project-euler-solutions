#pragma once
#define DAYS_IN_A_WEEK 7
#define MONTHS_IN_A_YEAR 12
typedef unsigned char byte;

enum Month {
	JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER
};

enum DayOfTheWeek {
	SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
};

class Date {
public:
	Date(const Date& other);
	Date(byte day, Month month, int year);

	Date& operator=(const Date& right);
	bool operator==(const Date& right) const;
	bool operator!=(const Date& right) const;
	bool operator>(const Date& right) const;
	bool operator<(const Date& right) const;
	Date& operator++();
	inline Date operator++(int);

	byte day() const;
	Month month() const;
	int year() const;
	DayOfTheWeek dayOfTheWeek() const;

	static bool isLeapYear(int year);

protected:
	const byte COMMON_YEAR_MONTH_LENGTHS[MONTHS_IN_A_YEAR] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	const byte LEAP_YEAR_MONTH_LENGTHS[MONTHS_IN_A_YEAR] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	enum CompareResult {
		EARLIER_THAN = -1, EQUALS_TO, LATER_THAN
	};
	CompareResult compare(const Date& other) const;

private:
	Date() = default;

	byte _day;
	Month _month;
	int _year;
	DayOfTheWeek _dayOfTheWeek;

	static Date knownSunday;
};

