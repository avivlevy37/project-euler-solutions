#include <iostream>
#include "Date.h"

int main() {
	Date start(1, JANUARY, 1901), end(31, DECEMBER, 2000);
	unsigned count = 0;
	while (start != end) {
		if (start.day() == 1 && start.dayOfTheWeek() == SUNDAY)
			count++;
		start++;
	}
	std::cout << count << std::endl;
	return 0;
}
