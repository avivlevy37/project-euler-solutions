#include <iostream>
#include <cmath>

bool isPrime(unsigned number) {
	if (number < 2)
		return false;
	unsigned stop = std::sqrt(number);
	for (unsigned i = 2; i <= stop; i++)
		if (number % i == 0)
			return false;
	return true;
}

int main() {
	unsigned long long sum = 0;
	for (unsigned i = 0; i < 2'000'000; i++)
		if (isPrime(i))
			sum += i;
	std::cout << sum << std::endl;
}
