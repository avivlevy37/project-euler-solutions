#include <iostream>
#include <map>
#include <cmath>
#include <algorithm>

/// <summary>
/// Receives a number and returns whether it is prime or composite.
/// </summary>
/// <param name="number"></param>
/// <returns></returns>
bool isPrime(unsigned long long number) {
	unsigned root = std::sqrt(number);

	if (number < 2) //0,1 aren't prime
		return false;

	for (unsigned i = 2; i <= root; i++)
		if (number % i == 0)
			return false;
	return true;
}


/// <summary>
/// Gets all the prime factors of a number.
/// </summary>
/// <param name="number"></param>
/// <returns>A map, with the key being the prime factor and the value being its exponent.</returns>
std::map<unsigned long long, unsigned> primeFactors(unsigned long long number) {
	std::map<unsigned long long, unsigned> primeFactors;

	for (unsigned i = 2; i <= number; i++) {
		unsigned power = 0;

		if (!isPrime(i))
			continue;
		while (number % i == 0) {
			power++;
			number /= i;
		}
		if (power)
			primeFactors.insert(std::pair<unsigned long long, unsigned>(i, power));
	}

	return primeFactors;
}


int main() {
	unsigned long long max = 0;
	for (const auto& pair : primeFactors(600851475143)) {
		if (pair.first > max)
			max = pair.first;
	}
	std::cout << max << std::endl;
	return 0;
}